/** @type {import("eslint").Linter.Config} */
module.exports = {
  root: true,
  extends: ["@uqai-test/eslint-config/index.js"],
};
